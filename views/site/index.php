<?php

use yii\helpers\Html;

/* @var yii\web\View $this */
/* @var \yii\db\BatchQueryResult $batchQueryResult */


$this->title = 'Numbers';
?>
<h1 style="margin-top: 100px;"><?= Html::encode($this->title) ?></h1>

<div class="row" style="white-space: nowrap;padding: 10px 5%;">
    <table>
        <?=
        \app\widgets\FixedTableWidget::widget([
            'batchQueryResult' => $batchQueryResult
        ]);
        ?>
    </table>
</div>


<div class="row top-divide" style="margin-left: 5%;">
    <?= Html::submitButton('Update Values', ['class' => 'btn btn-primary update-values-btn']) ?>
</div>