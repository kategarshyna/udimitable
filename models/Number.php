<?php

namespace app\models;

use yii\db\ActiveRecord;


/**
 * @property integer      $row
 * @property integer      $column
 * @property integer      $value
 */

class Number extends ActiveRecord
{
    const NUMBERS_ROWS = 100;
    const NUMBERS_COLUMNS = 100;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'numbers';
    }

    public static function getValueByPosition ($row, $column) {
        return Number::find()->where(['row' => $row])->andWhere(['column' => $column])->one();
    }
}
