<?php

namespace app\controllers;

use app\models\Number;
use Yii;
use yii\base\Exception;
use yii\web\Controller;


class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $batchQueryResult = Number::find()
            ->orderBy(['row' => SORT_ASC, 'column' => SORT_ASC])
            ->asArray()
            ->batch(Number::NUMBERS_ROWS);

        return $this->render('index', [
            'batchQueryResult' => $batchQueryResult,
        ]);
    }

    public function actionUpdate ()
    {
        $data = [];
        foreach ((array) Yii::$app->request->post('data') as $item) {
            $data[] = [
                'row' => $item['row'],
                'column' => $item['column'],
                'value' => $item['value'],
            ];
        }
        if (!empty($data)) {
            $command = Number::getDb()->createCommand()->batchInsert(
                Number::tableName(),
                ['row', 'column', 'value'],
                $data
            );

            Number::getDb()->createCommand(
                $command->getRawSql()
                . " ON DUPLICATE KEY UPDATE [[value]] = VALUES([[value]])"
            )->execute();
        }

        return true;
    }
}
