(function () {
   var changes = [];

    $('.number-item').on('change', function() {
        var value = $(this).val();
        var row = $(this).data('row');
        var column = $(this).data('column');

        changes.push({row: row, column: column, value: value});
    });

    $('.update-values-btn').on('click', function() {
        $.post( "/site/update", { csrf: yii.getCsrfToken(), data: changes })
            .done(function(  ) {
                window.location.reload();
            })
            .error(function () {
                alert('Internal error');
            });
    });
})();

