<?php

namespace app\widgets;

use app\models\Number;
use yii\base\Widget;
use yii\helpers\Html;

class FixedTableWidget extends Widget
{
    public $batchQueryResult = [];

    public function run()
    {
        $i = 1;
        $from = ['row' => 1, 'column' => 1];
        foreach ($this->batchQueryResult as $batch) {
            $this->iterateBatch($from, $batch, $i);
        }
        $last = [['row' => Number::NUMBERS_ROWS, 'column' => Number::NUMBERS_COLUMNS]];
        $this->iterateBatch($from, $last, $i);
    }

    /**
     * @param array $from
     * @param array $to
     * @return array[]
     */
    function getInterval(array $from, array $to)
    {
        $interval = [];

        $row = (int) $from['row'];
        $l = floor($to['row'] - $from['row']) * Number::NUMBERS_COLUMNS;
        if ($l) {
            $toCol = (int) $l + (int) $to['column'];
        } else {
            $toCol = (int) $to['column'];
        }

        for ($col = (int) $from['column']; $col <= $toCol; $col++ ) {
            if ($col > Number::NUMBERS_COLUMNS && $col % Number::NUMBERS_COLUMNS) {
                $colStr = $col % Number::NUMBERS_COLUMNS;
            } else {
                $colStr = $col;
            }

            if ($col !== $toCol || ($colStr == Number::NUMBERS_COLUMNS && $row === Number::NUMBERS_ROWS)) {
                if ((int) $from['column'] === (int) $colStr && (int) $from['row'] === (int) $row && !isset($interval[$row . '.' . $colStr])) {
                    $interval[$row . '.' . $colStr] = $from;
                } elseif (!isset($interval[$row . '.' . $colStr])) {
                    $interval[$row . '.' . $colStr] = [
                        'row' => $row,
                        'column' => $colStr,
                    ];
                }
            }

            if ($col % Number::NUMBERS_COLUMNS === 0) {
                $row++;
            }
        }

        return $interval;
    }

    function iterateBatch(&$from, &$batch, &$i)
    {
        /** @var array $model */
        foreach ($batch as $model) {
            $models = $this->getInterval($from, $model);

            foreach ($models as $num) { ?>
                <div class="table-cell">
                    <?= Html::input('text', "item", isset($num['value']) ? $num['value'] : '', [
                        'class' => 'number-item',
                        'data-row' => $num['row'],
                        'data-column' => $num['column'],
                        'type' => 'number',
                        'min' => 1,
                        'max' => 99999
                    ]) ?>
                </div>
                <?php if($num['column'] % Number::NUMBERS_COLUMNS === 0) { ?>
                    <div></div>
                <?php }
                $i++;
            }

            $from = $model;
        }
    }
}