<?php

use yii\db\Migration;

class m170707_010000_numbers_table_create extends Migration
{
    public function safeUp()
    {
        $this->createTable('numbers', [
            'row' => $this->integer(6)->unsigned(),
            'column' => $this->integer(6)->unsigned(),
            'value' => $this->integer(6)->unsigned()
        ]);
        $this->addPrimaryKey('pk_row_col', 'numbers', ['row', 'column']);
    }

    public function safeDown()
    {
        $this->dropTable('numbers');
    }
}
